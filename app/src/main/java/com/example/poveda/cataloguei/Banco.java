package com.example.poveda.cataloguei;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Poveda on 02/11/2016.
 */

public class Banco extends SQLiteOpenHelper
{
    private static final String Nome_Banco = "catalogo.db";
    private static final int Versao = 1;

    public Banco(Context context)
    {
        super(context, Nome_Banco, null, Versao);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        try 
        {
            String sql = "CREATE TABLE tipos( " +
                    "id integer primary key autoincrement NOT NULL, " +
                    "TipoNome text NOT NULL)";
            //Criar tabela de Tipos
            sqLiteDatabase.execSQL(sql);

            sql = "CREATE TABLE colecoes( " +
                    "id integer primary key autoincrement NOT NULL, " +
                    "idColecaoPai integer NULL, " +
                    "idTipo integer NOT NULL, " +
                    "ColecaoNome text NOT NULL, " +
                    "Descricao text NULL,"+
                    "foreign key(idTipo) references tipos(id)" +
                    "foreign key(idColecaoPai) references colecoes(id))";
            //Criar tabela de coleções
            sqLiteDatabase.execSQL(sql);

            sql = "CREATE TABLE items( " +
                    "id integer primary key autoincrement NOT NULL," +
                    "idColecao integer NOT NULL, " +
                    "ItemNome text NOT NULL, " +
                    "Codigo text NULL, " +
                    "Descricao text NULL, " +
                    "ValorItem numeric NULL," +
                    "foreign key(idColecao) references colecoes(id))";
            //Criar tabela de itens
            sqLiteDatabase.execSQL(sql);
        }
        catch(Exception e)
        {

    }
        
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1)
    {
        sqLiteDatabase.execSQL("Drop Table if exists items");
        sqLiteDatabase.execSQL("Drop Table if exists colecoes");
        sqLiteDatabase.execSQL("Drop Table if exists tipos");
        onCreate(sqLiteDatabase);
    }
}
