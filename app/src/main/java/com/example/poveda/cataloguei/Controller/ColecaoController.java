package com.example.poveda.cataloguei.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.poveda.cataloguei.Banco;
import com.example.poveda.cataloguei.Model.Colecao;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

/**
 * Created by Poveda on 06/11/2016.
 */

public class ColecaoController {
    private SQLiteDatabase _db;
    private Banco _banco;

    public ColecaoController(Context context)
    {
        _banco = new Banco(context);
    }

    public boolean Add(Integer idColecaoPai, int idTipo, String colecaoNome, String descricao)
    {
        boolean resultado;
        ContentValues values = new ContentValues();
        _db = _banco.getWritableDatabase();
        values.put("idColecaoPai", idColecaoPai);
        values.put("idTipo", idTipo);
        values.put("ColecaoNome", colecaoNome);
        values.put("Descricao", descricao);

        resultado = _db.insert("colecoes", null, values) != -1;

        //Fecha o banco
        _db.close();
        return resultado;
    }

    public void Update(int id, Integer idColecaoPai,int idTipo, String colecaoNome, String descricao)
    {
        ContentValues values = new ContentValues();
        String where = "id = " + id;
        _db = _banco.getWritableDatabase();

        values.put("idColecaoPai", idColecaoPai);
        values.put("idTipo", idTipo);
        values.put("ColecaoNome", colecaoNome);
        values.put("Descricao", descricao);

        _db.update("colecoes", values, where, null);

        //Fecha o banco
        _db.close();
    }

    public void Delete(int id)
    {
        String where = "id = " + id;
        _db = _banco.getWritableDatabase();

        _db.delete("colecoes", where, null);

        //Fecha o banco
        _db.close();
    }

    //Pode rolar sql injection
    //Select id, idColecaoPai, idTipo, ColecaoNome, Descricao from item limit 10
    public Cursor Retrieve(String sql)
    {
        Cursor cursor;

        _db = _banco.getReadableDatabase();
        cursor = _db.rawQuery(sql, null);
        cursor.moveToFirst();
        _db.close();

        return cursor;
    }

    public Cursor GetAll(int take)
    {
        Cursor cursor;
        String[] campos = {"id", "idColecaoPai", "idTipo", "ColecaoNome", "Descricao"};
        String limit = String.valueOf(take);

        _db = _banco.getReadableDatabase();
        cursor = _db.query("colecoes",campos, null,null, null, null, null, limit);
        cursor.moveToFirst();
        _db.close();

        return cursor;
    }

    public ArrayList<Colecao> GetAll()
    {
        Cursor cursor;
        String[] campos = {"id", "idColecaoPai", "idTipo", "ColecaoNome", "Descricao"};

        _db = _banco.getReadableDatabase();
        cursor = _db.query("colecoes",campos, null,null, null, null,null);

        ArrayList<Colecao> colecaoArrayList = AutoMapper(cursor);

        _db.close();

        return colecaoArrayList;
    }

    //https://stackoverflow.com/questions/14468586/efficient-paging-in-sqlite-with-millions-of-records
    //https://developer.android.com/reference/android/database/sqlite/SQLiteQueryBuilder.html
    public ArrayList<Colecao> GetAllPaging(int skip, int take)
    {
        Cursor cursor;
        String[] campos = {"id", "idColecaoPai", "idTipo", "ColecaoNome", "Descricao"};
        String limit = "Limit" + take + "," + skip;
        _db = _banco.getReadableDatabase();
        cursor = _db.query("colecoes",campos, null,null, null, null,null);

        _db.close();
        return AutoMapper(cursor);
    }

    public Colecao GetByID(int id)
    {
        Cursor cursor;
        String[] campos = {"id", "idColecaoPai", "idTipo", "ColecaoNome", "Descricao"};
        String where = "id = " + id;
        _db = _banco.getReadableDatabase();
        cursor = _db.query("colecoes",campos, where,null, null, null,null);
        Colecao colecao = AutoMapper(cursor).get(0);

        _db.close();
        return colecao;
    }

    //Mapeia uma lista
    public ArrayList<Colecao> AutoMapper(Cursor cursor)
    {
        ArrayList<Colecao> data = new ArrayList<>();

        if(cursor!= null && cursor.getCount() > 0)
        {
            //cursor.moveToFirst();
            while (cursor.moveToNext())
            {
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                int idColecaoPai = cursor.getInt(cursor.getColumnIndex("idColecaoPai"));
                int idTipo = cursor.getInt(cursor.getColumnIndex("idTipo"));
                String colecaoNome = cursor.getString(cursor.getColumnIndex("ColecaoNome"));
                String descricao = cursor.getString(cursor.getColumnIndex("Descricao"));

                Colecao colecao = new Colecao(id, idColecaoPai, idTipo, colecaoNome, descricao);

                //Se der erro não tem problema
                if(cursor.getColumnIndex("ColecaoNomePai") > -1) {
                    String colecaoPaiNome = cursor.getString(cursor.getColumnIndex("ColecaoNomePai"));
                    String itemNome = cursor.getString(cursor.getColumnIndex("TipoNome"));
                    colecao.SetColecaoPaiNome(colecaoPaiNome);
                    colecao.SetTipoNome(itemNome);
                }

                data.add(colecao);
            }
        }

        return data;
    }
}
