package com.example.poveda.cataloguei.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.poveda.cataloguei.Banco;
import com.example.poveda.cataloguei.Model.Item;

import java.util.ArrayList;

/**
 * Created by Poveda on 06/11/2016.
 */

public class ItemController
{
    private SQLiteDatabase _db;
    private Banco _banco;

    public ItemController(Context context)
    {
        _banco = new Banco(context);
    }

    public boolean Add(int idColecao, String itemNome, String codigo, String descricao, double valorItem)
    {
        boolean resultado;
        ContentValues values = new ContentValues();

        _db = _banco.getWritableDatabase();
        values.put("idColecao", idColecao);
        values.put("ItemNome", itemNome);
        values.put("Codigo", codigo);
        values.put("Descricao", descricao);
        values.put("ValorItem", valorItem);

        resultado = _db.insert("items", null, values) != -1;

        //Fecha o banco
        _db.close();
        return resultado;
    }

    public void Update(int id, int idColecao, String itemNome, String codigo, String descricao, double valorItem)
    {
        ContentValues values = new ContentValues();
        String where = "id = " + id;
        _db = _banco.getWritableDatabase();

        values.put("idColecao", idColecao);
        values.put("ItemNome", itemNome);
        values.put("Codigo", codigo);
        values.put("Descricao", descricao);
        values.put("ValorItem", valorItem);

        _db.update("items", values, where, null);

        //Fecha o banco
        _db.close();
    }

    public void Delete(int id)
    {
        String where = "id = " + id;
        _db = _banco.getWritableDatabase();

        _db.delete("items", where, null);

        //Fecha o banco
        _db.close();
    }

    //Pode rolar sql injection
    public Cursor Retrieve(String sql)
    {
        Cursor cursor;
        _db = _banco.getReadableDatabase();
        cursor = _db.rawQuery(sql, null);
        if(cursor!= null)
            cursor.moveToFirst();
        _db.close();
        return cursor;
    }

    public ArrayList<Item> GetAll()
    {
        Cursor cursor;
        String[] campos = {"id", "idColecao", "ItemNome", "Codigo", "Descricao", "ValorItem"};

        _db = _banco.getReadableDatabase();
        cursor = _db.query("items",campos, null,null, null, null,null);
        ArrayList<Item> itemArrayList = AutoMapper(cursor);

        _db.close();

        return itemArrayList;
    }

    public Item GetByID(int id)
    {
        Cursor cursor;
        String[] campos = {"id", "idColecao", "ItemNome", "Codigo", "Descricao", "ValorItem"};

        String where = "id = " + id;
        _db = _banco.getReadableDatabase();
        cursor = _db.query("colecoes",campos, where,null, null, null,null);

        Item item = AutoMapper(cursor).get(0);

        _db.close();
        return item;
    }

    //Mapea uma lista
    public ArrayList<Item> AutoMapper(Cursor cursor)
    {
        ArrayList<Item> data = new ArrayList<>();

        if(cursor!= null && cursor.getCount() > 0)
        {
            while (cursor.moveToNext())
            {
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                int idColecao = cursor.getInt(cursor.getColumnIndex("idColecao"));
                String itemNome = cursor.getString(cursor.getColumnIndex("ItemNome"));
                String codigo = cursor.getString(cursor.getColumnIndex("Codigo"));
                String descricao = cursor.getString(cursor.getColumnIndex("Descricao"));
                double valorItem = cursor.getDouble(cursor.getColumnIndex("ValorItem"));

                Item item = new Item(id, idColecao, itemNome, codigo, descricao, valorItem);

                if(cursor.getColumnIndex("ColecaoNome")> -1 ) {
                    String colecaoNome = cursor.getString(cursor.getColumnIndex("ColecaoNome"));
                    item.SetColecaoNome(colecaoNome);
                }

                data.add(item);
            }
        }

        return data;
    }
}
