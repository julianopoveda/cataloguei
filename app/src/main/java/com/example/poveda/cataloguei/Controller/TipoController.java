package com.example.poveda.cataloguei.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.poveda.cataloguei.Banco;
import com.example.poveda.cataloguei.Model.Tipo;

import java.util.ArrayList;

/**
 * Created by Poveda on 02/11/2016.
 * links referencia
 * http://www.devmedia.com.br/criando-um-crud-com-android-studio-e-sqlite/32815
 * https://developer.android.com/reference/android/database/sqlite/SQLiteDatabase.html
 *
 */

public class TipoController
{
    private SQLiteDatabase _db;
    private Banco _banco;

    public TipoController(Context context)
    {
        _banco = new Banco(context);
    }

    public boolean Add(String tipoNome)
    {
        boolean resultado;
        ContentValues values = new ContentValues();
        _db = _banco.getWritableDatabase();
        values.put("TipoNome", tipoNome);

        // Mensagens: "Registro Inserido":"Erro ao inserir registro";
        resultado = _db.insert("tipos", null, values) != -1;

        //Fecha o banco
        _db.close();
        return resultado;
    }

    public void Update(int id, String tipoNome)
    {
        ContentValues values = new ContentValues();
        String where = "id = " + id;
        _db = _banco.getWritableDatabase();
        values.put("TipoNome", tipoNome);

        _db.update("tipos", values, where, null);

        //Fecha o banco
        _db.close();
    }

    public void Delete(int id)
    {
        String where = "id = " + id;
        _db = _banco.getWritableDatabase();

        _db.delete("tipos", where, null);

        //Fecha o banco
        _db.close();
    }

    //Pode rolar sql injection
    public Cursor Retrieve(String sql)
    {
        Cursor cursor;
        _db = _banco.getReadableDatabase();
        cursor = _db.rawQuery(sql, null);
        if(cursor!= null)
            cursor.moveToFirst();
        _db.close();
        return cursor;
    }

    public ArrayList<Tipo> GetAll()
    {
        Cursor cursor;
        String[] campos = {"id", "TipoNome"};

        _db = _banco.getReadableDatabase();
        cursor = _db.query("tipos",campos, null,null, null, null,null);
        ArrayList<Tipo> tipoArrayList = AutoMapper(cursor);

        _db.close();

        return tipoArrayList;
    }

    public Tipo GetByID(int id)
    {
        Cursor cursor;
        String[] campos = {"ID", "TipoNome"};
        String where = "id = " + id;
        _db = _banco.getReadableDatabase();
        cursor = _db.query("Tipos",campos, where,null, null, null,null);

        Tipo tipo = AutoMapper(cursor).get(0);

        _db.close();
        return tipo;
    }

    private ArrayList<Tipo> AutoMapper(Cursor cursor)
    {
        ArrayList<Tipo> data = new ArrayList<>();
        if(cursor!= null && cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            while (cursor.moveToNext())
            {
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                String tipoNome = cursor.getString(cursor.getColumnIndex("TipoNome"));

                data.add(new Tipo(id, tipoNome));
            }
        }
        return data;
    }
}


//    public Cursor GetAll()
//    {
//        Cursor cursor;
//        String[] campos = {"TipoNome"};
//        _db = _banco.getReadableDatabase();
//        cursor = _db.query("tipos",campos, null,null, null, null,null);
//
//        if(cursor!= null)
//            cursor.moveToFirst();
//        _db.close();
//        return cursor;
//    }
