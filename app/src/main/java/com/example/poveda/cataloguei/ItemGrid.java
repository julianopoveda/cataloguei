package com.example.poveda.cataloguei;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.example.poveda.cataloguei.Controller.ItemController;
import com.example.poveda.cataloguei.LayoutAdapters.ItemGridAdapter;

public class ItemGrid extends AppCompatActivity {

    private GridView _grid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_grid);

        _grid = (GridView) findViewById(R.id.gridItemold);

        ItemController controller = new ItemController(getBaseContext());
        //TODO: Otimizar esta consulta. Dá erro no automapper se não tiver todos os campos
        String sql = "select items.id, items.idColecao, col.colecaonome as ColecaoNome, items.ItemNome, items.Codigo, items.Descricao, items.ValorItem " +
                "from items " +
                "left join colecoes col on col.id = items.idColecao ";

        ItemGridAdapter adapter = new ItemGridAdapter(this, controller.AutoMapper(controller.Retrieve(sql)));
        _grid.setAdapter(adapter);
    }

    public void EditarItem(View view)
    {
        TextView idTxt = (TextView)view.findViewById(R.id.iidentifier);
        Integer id = Integer.parseInt(idTxt.getText().toString());

        Intent intent = new Intent(this, itemeditform.class);
        intent.putExtra("idItem", id);
        startActivity(intent);
    }
}
