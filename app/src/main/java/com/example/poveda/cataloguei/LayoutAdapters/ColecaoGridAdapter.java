package com.example.poveda.cataloguei.LayoutAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.poveda.cataloguei.Model.Colecao;
import com.example.poveda.cataloguei.R;

import java.util.ArrayList;

/**
 * Created by Poveda on 06/11/2016.
 */

public class ColecaoGridAdapter extends BaseAdapter {
    private Context _context;
    private ArrayList<Colecao> _data;
    private static LayoutInflater _inflater = null;

    public ColecaoGridAdapter(Context context, ArrayList<Colecao> data)
    {
        _context = context;
        _data = data;
        _inflater = (LayoutInflater)context.getSystemService(_context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        return _data.size();
    }

    @Override
    public Object getItem(int item)
    {
        return item;
    }

    @Override
    public long getItemId(int item)
    {
        return item;//TODO melhorar
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup)
    {
        if(view == null)
            view = _inflater.inflate(R.layout.activity_layout_grid_item_colecao, null);

        TextView id = (TextView)view.findViewById(R.id.cidentifier);
        TextView colecaoPai = (TextView)view.findViewById(R.id.colecaoPaigrid);
        TextView tipo = (TextView)view.findViewById(R.id.colecaotipoGrid);
        TextView colecaoNome = (TextView)view.findViewById(R.id.colecaonomegrid);
        TextView descricao = (TextView)view.findViewById(R.id.colecaodescricaogrid);

        Colecao colecao = _data.get(position);

        id.setText(String.valueOf(colecao.GetID()));
        colecaoPai.setText(colecao.GetColecaoPaiNome());
        tipo.setText(colecao.GetTipo());
        colecaoNome.setText(colecao.GetColecaoNome());
        descricao.setText(colecao.GetDescricao());

        return view;
    }
}
