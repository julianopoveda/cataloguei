package com.example.poveda.cataloguei.LayoutAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.poveda.cataloguei.Model.Item;
import com.example.poveda.cataloguei.R;

import java.util.ArrayList;

/**
 * Created by Poveda on 06/11/2016.
 */

public class ItemGridAdapter extends BaseAdapter
{
    private Context _context;
    private ArrayList<Item> _data;
    private static LayoutInflater _inflater = null;

    public ItemGridAdapter(Context context, ArrayList<Item> data)
    {
        _context = context;
        _data = data;
        _inflater = (LayoutInflater)context.getSystemService(_context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        return _data.size();
    }

    @Override
    public Object getItem(int item)
    {
        return item;
    }

    @Override
    public long getItemId(int item)
    {
        return item;//TODO melhorar
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup)
    {
        if(view == null)
            view = _inflater.inflate(R.layout.layout_grid_item, null);

        TextView id = (TextView)view.findViewById(R.id.iidentifier);
        TextView colecao = (TextView)view.findViewById(R.id.colecaoGrid);
        TextView itemNome = (TextView)view.findViewById(R.id.itemnomeGrid);
        TextView codigo = (TextView)view.findViewById(R.id.itemcodigoGrid);
        TextView descricao = (TextView)view.findViewById(R.id.itemdescricaoGrid);
        TextView valorItem = (TextView)view.findViewById(R.id.itemvalorGrid);

        Item item = _data.get(position);

        id.setText(String.valueOf(item.GetID()));
        colecao.setText(String.valueOf(item.GetColecaoNome()));
        itemNome.setText(item.GetItemNome());
        codigo.setText(item.GetCodigo());
        descricao.setText(item.GetDescricao());
        valorItem.setText(String.valueOf(item.GetValorItem()));

        return view;
    }
}
