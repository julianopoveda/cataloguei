package com.example.poveda.cataloguei;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.poveda.cataloguei.Controller.ItemController;
import com.example.poveda.cataloguei.LayoutAdapters.ItemGridAdapter;
import com.example.poveda.cataloguei.Model.Colecao;

import java.util.HashMap;

public class Main2Activity extends AppCompatActivity {

    private ListView _menuLateral;
    private ArrayAdapter<String> _menuItens;
    private HashMap<String, Class<?>> _menuActions;
    private GridView _grid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        _menuLateral = (ListView)findViewById(R.id.menuLateral);

        InicializaMenu();

        _menuLateral.setAdapter(_menuItens);

        _menuLateral.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView selectedItem = (TextView) view;

                String intentName = selectedItem.getText().toString();
                SetIntent(intentName);
            }
        });


//        _grid = (GridView) findViewById(R.id.gridItem);
//
//        ItemController controller = new ItemController(getBaseContext());
//        //TODO: Otimizar esta consulta. Dá erro no automapper se não tiver todos os campos
//        String sql = "select items.id, items.idColecao, col.colecaonome as ColecaoNome, items.ItemNome, items.Codigo, items.Descricao, items.ValorItem " +
//                "from items " +
//                "left join colecoes col on col.id = items.idColecao ";
//
//        ItemGridAdapter adapter = new ItemGridAdapter(this, controller.AutoMapper(controller.Retrieve(sql)));
//        _grid.setAdapter(adapter);
    }

    private void InicializaMenu()
    {
        _menuItens = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);

        _menuItens.add("Coleção");
        _menuItens.add("Cadastrar Coleção");
        _menuItens.add("Cadastrar Tipos");
        _menuItens.add("Itens");
        _menuItens.add("Cadastrar Itens");

        _menuActions = new HashMap<>();

        _menuActions.put("Coleção", Colecao.class);
        _menuActions.put("Cadastrar Coleção",colecaoeditform.class);
        _menuActions.put("Cadastrar Tipos", Tipos.class);
        _menuActions.put("Itens", ItemGrid.class);
        _menuActions.put("Cadastrar Itens", itemeditform.class);
    }

    private void SetIntent(String intentName)
    {
        Intent intent = new Intent(this , _menuActions.get(intentName));
        startActivity(intent);
    }


    public void OpenTipo(View view)
    {
        Intent intent = new Intent(this, Tipos.class);
        startActivity(intent);
    }

    public void OpenColecao(View view)
    {
        Intent intent = new Intent(this, colecaoeditform.class);
        startActivity(intent);
    }

    public void OpenGridColecao(View view) {
        Intent intent = new Intent(this, colecaoGrid.class);
        startActivity(intent);
    }

    public void OpenItem(View view) {
        Intent intent = new Intent(this, itemeditform.class);
        startActivity(intent);
    }

    public void OpenGridItem(View view)
    {
        Intent intent = new Intent(this, ItemGrid.class);
        startActivity(intent);
    }
}
