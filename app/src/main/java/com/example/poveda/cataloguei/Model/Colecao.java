package com.example.poveda.cataloguei.Model;

/**
 * Created by Poveda on 06/11/2016.
 */

public class Colecao
{
    int ID;
    Integer IdColecaoPai;
    String ColecaoPaiNome;
    int IdTipo;
    String TipoNome;
    String ColecaoNome;
    String Descricao;

    //Construtor padrão precisa existir
    public Colecao(){}

    //Agilizar o preenchimento de listas
    public Colecao(int id, Integer idColecaoPai,int idTipo, String colecaoNome, String descricao)
    {
        ID = id;
        IdColecaoPai = idColecaoPai;
        IdTipo = idTipo;
        ColecaoNome = colecaoNome;
        Descricao = descricao;
    }

    public int GetID() {return ID;}
    public int GetIDColecaoPai() {return IdColecaoPai;}
    public String GetColecaoPaiNome() {return ColecaoPaiNome;}
    public int GetIDTipo() {return IdTipo;}
    public String GetTipo() {return TipoNome;}
    public String GetColecaoNome() {return ColecaoNome;}
    public String GetDescricao() {return Descricao;}

    public void SetID(int id) {ID = id;}
    public void SetIDColecaoPai(int id) {IdColecaoPai = id;}
    public void SetColecaoPaiNome(String colecaoNome) {ColecaoPaiNome = colecaoNome;}
    public void SetIDTipo(int id) {IdTipo = id;}
    public void SetTipoNome(String tipoNome) {TipoNome = tipoNome;}
    public void SetColecaoNome(String colecaoNome) {ColecaoNome = colecaoNome;}
    public void SetDescricao(String descricao) {Descricao = descricao;}
}
