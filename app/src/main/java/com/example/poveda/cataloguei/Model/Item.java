package com.example.poveda.cataloguei.Model;

/**
 * Created by Poveda on 06/11/2016.
 */

public class Item
{
    int ID;
    int IdColecao;
    String ColecaoNome;
    String ItemNome;
    String Codigo;
    String Descricao;
    double ValorItem;

    public Item() {}

    public Item(int id, int idColecao, String itemNome, String codigo, String descricao, double valorItem)
    {
        ID = id;
        IdColecao = idColecao;
        ItemNome = itemNome;
        Codigo = codigo;
        Descricao = descricao;
        ValorItem = valorItem;
    }

    public int GetID() {return ID;}
    public int GetIDColecao() {return IdColecao;}
    public String GetColecaoNome() {return ColecaoNome;}
    public String GetItemNome() {return ItemNome;}
    public String GetCodigo() {return Codigo;}
    public String GetDescricao() {return Descricao;}
    public double GetValorItem() {return ValorItem;}

    public void SetID(int id) {ID = id;}
    public void SetIDColecao(int id) {IdColecao = id;}
    public void SetColecaoNome(String colecaoNome) {ColecaoNome = colecaoNome;}
    public void SetItemNome(String itemNome) {ItemNome = itemNome;}
    public void SetCodigo(String codigo) {Codigo = codigo;}
    public void SetDescricao(String descricao) {Descricao = descricao;}
    public void SetValorItem(double valorItem) {ValorItem = valorItem;}
}
