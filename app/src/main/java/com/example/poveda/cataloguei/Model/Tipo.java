package com.example.poveda.cataloguei.Model;

/**
 * Created by Poveda on 06/11/2016.
 */

public class Tipo
{
    int ID;
    String TipoNome;

    //Construtor padrão precisa existir
    public Tipo(){}

    //Agilizar o preenchimento de listas
    public Tipo(int id, String tipoNome)
    {
        ID = id;
        TipoNome = tipoNome;
    }

    public int GetID() {return ID;}
    public String GetTipoNome() {return TipoNome;}

    public void SetID(int id) {ID = id;}
    public void SetTipoNome(String tipoNome) {TipoNome = tipoNome;}
}
