package com.example.poveda.cataloguei;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.poveda.cataloguei.Controller.TipoController;

public class Tipos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipos);
    }

    public void salvarTipo(View view)
    {
        TipoController tipoController = new TipoController(getBaseContext());
        EditText tipoDesc = (EditText)findViewById(R.id.descricao);
        String descricao = tipoDesc.getText().toString();

        if(!tipoController.Add(descricao))
            Toast.makeText(getApplicationContext(), "Erro ao adicionar o registro", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(getApplicationContext(), "Registro adicionado com sucesso", Toast.LENGTH_LONG).show();
    }
}
