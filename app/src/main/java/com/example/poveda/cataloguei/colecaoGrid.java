package com.example.poveda.cataloguei;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.example.poveda.cataloguei.Controller.ColecaoController;
import com.example.poveda.cataloguei.LayoutAdapters.ColecaoGridAdapter;
import com.example.poveda.cataloguei.LayoutAdapters.GridAdapter;

public class colecaoGrid extends AppCompatActivity {

    private GridView _grid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colecao_grid);

        _grid = (GridView) findViewById(R.id.gridcolecao);

        ColecaoController controller = new ColecaoController(getBaseContext());
        //TODO: Otimizar esta consulta. Dá erro no automapper se não tiver todos os campos
        String sql = "select colecoes.id, col.colecaonome as ColecaoNomePai, tipos.TipoNome, colecoes.colecaonome, colecoes.descricao, colecoes.idColecaoPai, colecoes.idTipo  " +
                "from colecoes " +
                "left join colecoes col on col.id = colecoes.idColecaoPai " +
                "left join tipos on colecoes.idTipo = tipos.id";

        ColecaoGridAdapter adapter = new ColecaoGridAdapter(this, controller.AutoMapper(controller.Retrieve(sql)));
        _grid.setAdapter(adapter);
    }

    public void EditarColecao(View view)
    {
        TextView idTxt = (TextView)view.findViewById(R.id.cidentifier);
        Integer id = Integer.parseInt(idTxt.getText().toString());

        Intent intent = new Intent(this, colecaoeditform.class);
        intent.putExtra("idColecao", id);
        startActivity(intent);
    }

}
