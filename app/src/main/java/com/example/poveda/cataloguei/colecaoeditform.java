package com.example.poveda.cataloguei;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.example.poveda.cataloguei.Controller.ColecaoController;
import com.example.poveda.cataloguei.Controller.TipoController;
import com.example.poveda.cataloguei.Model.Colecao;
import com.example.poveda.cataloguei.Model.Tipo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class colecaoeditform extends AppCompatActivity {

    private ColecaoController _controller;
    private Integer _idColecao;
    Spinner _idColecaoPai;
    Spinner _idTipo;
    EditText _descricao;
    EditText _colecao;
    TextView _resultado;

    private HashMap<String, Integer> _tipoHashMap;
    private HashMap<String, Integer> _colecaoHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colecaoeditform);

        _controller = new ColecaoController(getBaseContext());

        _idColecao = getIntent().getIntExtra("idColecao", 0);
        _idColecaoPai = (Spinner) findViewById(R.id.colecaopaispinner);
        _idTipo = (Spinner) findViewById(R.id.tipospinner);
        _descricao = (EditText) findViewById(R.id.colecaodescricao);
        _colecao = (EditText) findViewById(R.id.colecaonome);
        _resultado = (TextView) findViewById(R.id.colecaoEditFormResultado);

        if(_idColecao != 0) {
            Colecao colecao = _controller.GetByID(_idColecao);
            _descricao.setText(colecao.GetDescricao());
            _colecao.setText(colecao.GetColecaoNome());
            CarregarListaTipo(colecao.GetIDTipo());
            CarregarListaColecaoPai(colecao.GetIDColecaoPai());
        }
        else {
            CarregarListaTipo(null);
            CarregarListaColecaoPai(null);
        }
    }

    private void CarregarListaColecaoPai(Integer idColecaoPaiSelected) {
        ColecaoController colecaoController = new ColecaoController(getBaseContext());
        List<Colecao> colecaoList = colecaoController.GetAll();
        _colecaoHashMap = new HashMap<>();
        ArrayList<String> colecaoArray = new ArrayList<>() ;

        colecaoArray.add(0,"Selecione");

        int position = 0;
        int count = 0;

        for (Colecao colecao: colecaoList) {
            _colecaoHashMap.put(colecao.GetColecaoNome(), colecao.GetID());
            colecaoArray.add(colecao.GetColecaoNome());

            if(idColecaoPaiSelected!= null) {
                if (idColecaoPaiSelected != colecao.GetID())
                    count++;
                else
                    position = count == colecaoList.size()? count : ++count;
            }
        }

        ArrayAdapter<String> colecaoArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, colecaoArray);
        colecaoArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        _idColecaoPai.setAdapter(colecaoArrayAdapter);

        _idColecaoPai.setSelection(position);

    }

    private void CarregarListaTipo(Integer idTipoSelected) {
        TipoController tpController = new TipoController(getBaseContext());
        List<Tipo> tipoList = tpController.GetAll();
        _tipoHashMap= new HashMap<>();
        ArrayList<String> tipoArray = new ArrayList<>() ;

        tipoArray.add(0,"Selecione");
        int position = 0;
        int count = 0;
        for (Tipo tipo: tipoList) {
            _tipoHashMap.put(tipo.GetTipoNome(), tipo.GetID());
            tipoArray.add(tipo.GetTipoNome());

            if(idTipoSelected!= null) {
                if (idTipoSelected != tipo.GetID())
                    count++;
                else
                    position = count == tipoList.size()? count : ++count;
            }
        }

        ArrayAdapter<String> tipoArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, tipoArray);
        tipoArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        _idTipo.setAdapter(tipoArrayAdapter);
        _idTipo.setSelection(position);
    }

    public void SalvarColecao(View view)
    {
        Integer colecaoPaiId = _colecaoHashMap.get(_idColecaoPai.getSelectedItem());
        Integer tipoId =  _tipoHashMap.get(_idTipo.getSelectedItem());

        if(_idColecao == 0)
            if(_controller.Add(colecaoPaiId,tipoId, _colecao.getText().toString(),_descricao.getText().toString())) {
                LimparCampos();
                _resultado.setText("Coleção Adicionada com Sucesso");
            }
            else
                _resultado.setText("Erro ao Adicionar a Coleção. Verifique os dados e tente novamente");

        else {
            _controller.Update(_idColecao, colecaoPaiId, tipoId, _colecao.getText().toString(), _descricao.getText().toString());
            _resultado.setText("Coleção Atualizada com Sucesso");
        }
    }

    private void LimparCampos()
    {
        _descricao.setText("");
        _colecao.setText("");

        _tipoHashMap.clear();
        CarregarListaTipo(null);

        _colecaoHashMap.clear();
        CarregarListaColecaoPai(null);
    }
}
