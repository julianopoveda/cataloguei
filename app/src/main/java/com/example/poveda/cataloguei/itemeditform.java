package com.example.poveda.cataloguei;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.poveda.cataloguei.Controller.ColecaoController;
import com.example.poveda.cataloguei.Controller.ItemController;
import com.example.poveda.cataloguei.Model.Colecao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class itemeditform extends AppCompatActivity {
    private ItemController _controller;
    private Integer _idItem;
    Spinner _idColecao;
    EditText _descricao;
    EditText _item;
    EditText _codigo;
    EditText _valorItem;

    TextView _resultado;

    private HashMap<String, Integer> _colecaoHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itemeditform);

        _controller = new ItemController(getBaseContext());

        _idItem = getIntent().getIntExtra("idItem", 0);
        _idColecao = (Spinner)findViewById(R.id.colecaospinner);
        _descricao = (EditText)findViewById(R.id.itemdescricao);
        _item = (EditText)findViewById(R.id.itemnome);
        _codigo = (EditText)findViewById(R.id.itemcodigo);
        _valorItem = (EditText)findViewById(R.id.itemvalor);

        _resultado = (TextView)findViewById(R.id.colecaoEditFormResultado);

        CarregarListaColecao();
    }

    private void CarregarListaColecao() {
        ColecaoController colecaoController = new ColecaoController(getBaseContext());
        List<Colecao> colecaoList = colecaoController.GetAll();
        _colecaoHashMap = new HashMap<>();
        ArrayList<String> colecaoArray = new ArrayList<>() ;

        colecaoArray.add(0,"Selecione");

        for (Colecao colecao: colecaoList) {
            _colecaoHashMap.put(colecao.GetColecaoNome(), colecao.GetID());
            colecaoArray.add(colecao.GetColecaoNome());
        }

        ArrayAdapter<String> colecaoArrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, colecaoArray);
        colecaoArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        _idColecao.setAdapter(colecaoArrayAdapter);
    }

    public void SalvarItem(View view) {
        Integer colecaoId = _colecaoHashMap.get(_idColecao.getSelectedItem());

        if(_idItem == 0)
            if(_controller.Add(colecaoId,_item.getText().toString(), _codigo.getText().toString(),
                    _descricao.getText().toString(), Double.valueOf(_valorItem.getText().toString()))) {
                LimparCampos();
                _resultado.setText("Item Adicionada com Sucesso");
            }
            else
                _resultado.setText("Erro ao Adicionar a Coleção. Verifique os dados e tente novamente");

        else {
            _controller.Update(_idItem, colecaoId,_item.getText().toString(), _codigo.getText().toString(),
                    _descricao.getText().toString(), Double.valueOf(_valorItem.getText().toString()));
            _resultado.setText("Coleção Atualizada com Sucesso");
        }
    }

    private void LimparCampos()
    {
        _descricao.setText("");
        _item.setText("");
        _codigo.setText("");
        _valorItem.setText("");

        _colecaoHashMap.clear();
        CarregarListaColecao();
    }
}
